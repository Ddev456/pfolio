import React from "react";
import styled from "styled-components";
import { DarkTheme } from "../components/Themes";
// import navbarlogo from "../assets/Images/navBarLogo.png";

const Logo = styled.div`
  // background-image: url("");
  width: 150px;
  height: 150px;
  left: 1rem;
  top: 1rem;
  z-index: 3;
`;

const LogoComponent = () => {
  return <Logo></Logo>;
};

export default LogoComponent;
